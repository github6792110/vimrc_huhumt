#!/usr/bin/env bash

session="mySession"
tmux has-session -t "$session" 2>/dev/null

if [ $? != 0 ]; then
    tmux new-session -s "$session" -d
    for i in `seq 1 5`
    do
        tmux new-window
        if [ $i -gt 3 ]; then
            tmux split-window -v -p 10
            tmux swap-pane -D
            tmux split-window -h
        fi
    done
fi

tmux attach-session -d
