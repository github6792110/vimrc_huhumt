#!/usr/bin/env bash

# check whether vim exist
type vim > /dev/null
if [ $? -gt 0 ]
then
    print "Make sure you have successfully installed vim"
fi

# check whether curl exist
type curl > /dev/null
if [ $? -gt 0 ]
then
    print "This script heavily rely on curl to download file, install it first"
fi

# get current user's home directory
home_directory=$(eval echo ~${SUDO_USER})

# use vim-plug to manage all vim plugins
curl -fLo $home_directory/.vim/autoload/plug.vim --create-dirs \
        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# use self-configuration for vim and tmux
# cp .vimrc .tmux.conf .clang-format $home_directory
curl -fLo $home_directory/.vimrc \
        https://raw.githubusercontent.com/huhumt/vimrc_huhumt/master/.vimrc
curl -fLo $home_directory/.tmux.conf \
        https://raw.githubusercontent.com/huhumt/vimrc_huhumt/master/.tmux.conf
curl -fLo $home_directory/.clang-format \
        https://raw.githubusercontent.com/huhumt/vimrc_huhumt/master/.clang-format
curl -fLo $home_directory/.astylerc \
        https://raw.githubusercontent.com/huhumt/vimrc_huhumt/master/.astylerc

# delete old plugged and create new one
if [ -d "$home_directory/.vim/plugged" ]
then
    rm -fr $home_directory/.vim/plugged
fi
mkdir -p $home_directory/.vim/plugged

# install necessary vim plugins automatic
vim -c "silent PlugInstall" -c "qa"

vim_mark_plugin_directory="$home_directory/.vim/plugged/Mark/plugin/mark.vim"
sed -i 's/hi MarkWord1  ctermbg=Cyan    /hi MarkWord1  ctermbg=LightRed/' \
        $vim_mark_plugin_directory

usr_local_bin_path="/usr/local/bin"
# get system info of kernal and os in lowercase
kernel_name=$(echo "$(uname -s)" | tr '[:upper:]' '[:lower:]')
os_name=$(echo "$(uname -o)" | tr '[:upper:]' '[:lower:]')
if [[ "$kernel_name" = *"mingw"* ]] || [[ "$os_name" = *"msys"* ]]
then
    curl -fLo /usr/share/mintty/themes/base16-eighties-mod \
            https://raw.githubusercontent.com/huhumt/vimrc_huhumt/master/demo/base16-eighties-mod
    curl -fLo $home_directory/.minttyrc \
            https://raw.githubusercontent.com/huhumt/vimrc_huhumt/master/.minttyrc
else
    old_string="trans_bin = \"$usr_local_bin_path\""
    usr_local_bin_path="$home_directory/.local/bin"
    new_string="trans_bin = \"$usr_local_bin_path\""
    echo $old_string $new_string $home_directory/.vimrc
    sed -i "s#$old_string#$new_string#g" "$home_directory/.vimrc"
    mkdir -p $usr_local_bin_path
    if [ -f $home_directory/.bash_profile ]
    then
        # DO NOT OVERWRITE PREVIOUS BAK FILE
        if [ -f $home_directory/.bash_profile.bak ]
        then
            cp $home_directory/.bash_profile.bak $home_directory/.bash_profile
        else
            cp $home_directory/.bash_profile $home_directory/.bash_profile.bak
        fi
    fi
    echo "export PATH=\${PATH}:$usr_local_bin_path" >> $home_directory/.bash_profile
fi

# nc: if file exist, do not download
# nv: do not print that much log
# P: re-direct to given directory
# wget -nc -nv -P $usr_local_bin_path/ git.io/trans
curl -fLo $usr_local_bin_path/trans git.io/trans

curl -fLo $usr_local_bin_path/code_backup.sh \
        https://raw.githubusercontent.com/huhumt/vimrc_huhumt/master/code_backup.sh
curl -fLo $usr_local_bin_path/string_replace.sh \
        https://raw.githubusercontent.com/huhumt/vimrc_huhumt/master/string_replace.sh
curl -fLo $usr_local_bin_path/code_format_clang.sh \
        https://raw.githubusercontent.com/huhumt/vimrc_huhumt/master/code_format_clang.sh

chmod 755 $usr_local_bin_path/code_backup.sh \
          $usr_local_bin_path/string_replace.sh \
          $usr_local_bin_path/code_format_clang.sh \
          $usr_local_bin_path/trans
