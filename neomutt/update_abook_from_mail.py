#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os

def read_abook(abook_filename):
    """
    read abook file to python dict
    """

    fd = open(abook_filename, 'r')
    start_flag = 0
    abook_dict = {}
    mail_flag =  "@codethink.co.uk"
    for line in fd:
        if "name=" in line.replace(" ", ""):
            name = line.split("=")[-1].rstrip()
        if "email=" in line.replace(" ", ""):
            email_list = line.replace(" ", "").split("=")[-1].rstrip().split(",")
            for email in email_list:
                if mail_flag in email:
                    break
            abook_dict[email] = {"name": name}
            start_flag = 1
        elif not line.rstrip():
            start_flag = 0
        elif start_flag > 0:
            key = line.split("=")[0].rstrip()
            val = line.split("=")[-1].rstrip().split(",")
            if len(val) == 1:
                val = val[0]
            abook_dict[email][key] = val
    fd.close()
    return abook_dict

def read_email(email_text):
    """
    parse email
    """

    email_list = []
    # for line in email_text.splitlines():
    for line in email_text:
        mail_flag =  "@codethink.co.uk"
        line_strip = line.lower().strip()
        if ((line_strip.startswith("from")
                or line_strip.startswith("to"))
            and mail_flag in line_strip):
            tmp_list = line.replace(",", " ").split()
            for tmp in tmp_list:
                tmp = tmp.strip(",-<>()")
                if mail_flag in tmp.lower():
                    mail_address = tmp.split(":")[-1].split("]")[-1]
                    if mail_address not in email_list:
                        email_list.append(mail_address)
    return email_list

def update_abook(abook_filename, abook_dict, email_list):
    """
    update abook based on email_list
    """

    num = len(abook_dict.keys())
    new_string = ""
    for email in email_list:
        if email not in abook_dict.keys():
            new_string = "[{num}]\n".format(num=num)
            name = email.split("@")[0].replace(".", " ").title()
            new_string += "name={name}\n".format(name=name)
            new_string += "email={email}\n\n".format(email=email)
            num += 1

    if new_string:
        fd = open(abook_filename, 'a')
        fd.write(new_string)
        fd.close()

if __name__ == "__main__":
    """
    main entry of the program
    """

    home = os.path.expanduser("~")
    abook_filename = os.path.join(home, ".config/neomutt/Codethink_addressbook")
    abook_dict = read_abook(abook_filename)
    email_list = read_email(sys.argv[1])
    update_abook(abook_filename, abook_dict, email_list)
